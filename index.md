---
---
## Search Models

Use the **full-text search** field in the top-left of each category's page to find models by a specific author or cellular process.

Moreover, each model is tagged with common **keywords** like model formalism which are displayed below each model description. Clicking these tags displays a list of other models (and further content on the Morpheus website) with the same tag.

## Cite Models

Each model (including contributed and built-in examples) is registered with a **unique and persistent ID of the format `M....`**. The model description page, the model's XML file and all further files connected with that model can be retrieved via a **persistent URL** of the format `https://identifiers.org/morpheus/M....`. Further details on referencing, tracking of changes to the model etc. can be found in the [FAQ](/faq/models/referencing/).

## Submit Models

You can also contribute to the Morpheus model repository and upload MorpheusML-encoded model files yourself.

In our [FAQ](/faq/models/submit/) you will find all further information about this, including a **detailed guide** that takes you step by step through the submission process.

## Browse Models

We distinguish models of three categories: