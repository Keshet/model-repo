---
date: "2019-11-11T18:59:00+01:00"
lastmod: "2020-11-24T12:58:00+01:00"

aliases:
- /examples/
- /models/built-in-examples/
- /models/Built-in-Examples/

summary: "Didactic example models that are built-in with the GUI menu and may serve as templates for own models."

menu:
  Built-in Examples:
    weight: -10
weight: 10
---
{{< figure src="featured.png" alt="Schematic image of a laptop" caption="[CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/) [Font Awesome](https://fontawesome.com/license)" width="30%" >}}

Under this category you will find **didactic example models** that are **built-in with the GUI menu** and may serve as templates for own models. Each example model is explained in more detail.

These built-in examples aim to

- demonstrate **key features** of Morpheus,
- show the **diversity of modeling approaches**,
- introduce the **Morpheus model description language**,
- provide **templates and inspiration** for your own models.