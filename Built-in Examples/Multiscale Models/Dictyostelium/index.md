---
MorpheusModelID: M0034

authors: [N. J. Savill, P. Hogeweg]
contributors: [F. Rost, A. Quintero, M. Myllykoski, A. Igolkina, A. Rohde O’Sullivan Freltoft, N. Dixit]

title: "Dictyostelium"
date: "2019-11-11T15:52:00+01:00"
lastmod: "2020-10-30T13:35:00+01:00"

aliases: [/examples/dictyostelium/]

menu:
  Built-in Examples:
    parent: Multiscale Models
    weight: 40
weight: 200
---
## Introduction

This model show chemotactic aggregation of Dictyostelium. It was constructed by students attending the [ECMI modeling week 2012](http://www.math.tu-dresden.de/essim2012/) in Dresden.

![](dicty.png "Aggregation of amoebas through chemotaxis towards waves of cAMP.")

## Description

This model shows an interesting coupling between CPM cells and reaction-diffusion PDE. Cell state depends on the perceived concentration of cAMP and determines whether a cell produces cAMP and whether it performs chemotaxis. The PDE is governed by a Fitzhugh-Nagumo-like model of an excitable medium, which causes traveling waves upon excitation. Chemotaxis through those waves causes cell aggregation. 

Background colors indicate the cAMP concentration. Cells are color-coded according to their phase:

- excitable/resting (yellow),
- excited/chemotactic (green),
- refractory/resting (red).

<div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/57214708?loop=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

## Reference

- N. J. Savill, P. Hogeweg: [Modelling Morphogenesis: From Single Cells to Crawling Slugs][savill-1997]. *J. theor. Biol.*, **184** (3): 229–235, 1997.
- F. Rost, A. Quintero, M. Myllykoski, A. Igolkina, A. Rohde O’Sullivan Freltoft, N. Dixit: Morphogenesis and Dynamics of Multicellular Systems. *ECMI Newsletter*, **52**, 2012.

[rost-2012]: http://www.mafy.lut.fi/EcmiNL/issues.php?action=viewart&ID=280
[savill-1997]: http://dx.doi.org/10.1006/jtbi.1996.0237