---
MorpheusModelID: M0033

authors: [J. E. Ferrell Jr., T. Y. Tsai, Q. Yang]

title: "ODEs in CPM Cells: Cell Cycle and Proliferation"
date: "2019-11-11T11:29:00+01:00"
lastmod: "2021-02-23T20:23:00+01:00"

aliases: [/examples/odes-in-cpm-cells-cell-cycle-and-proliferation/]

menu:
  Built-in Examples:
    parent: Multiscale Models
    weight: 30
weight: 190

tags:
- Cell Cycle
- CellTypes
- CPM
- DOI:10.1016/j.cell.2011.03.006
- Gnuplotter
- MCSDuration
- ODE
- Proliferation
- Time-Scaling
- Time-Step
- Xenopus
---
>ODE model of the *Xenopus* oocyte cell cycle coupled to 2D shaped CPM cells that perform divide based on component concentrations

## Introduction

This multiscale model example shows

1. how to define a coupled system of continuous ODEs in discrete CPM cells,
1. how to specify and change time scales between these model formalisms.

![](cell-cycle.png "Cells divide according to an oscillatory ODE model representing the early cell cycle in *Xenopus*.")

## Description

This model specifies an oscillatory ODE model representing the cell cycle in Xenopus oocytes using three components ($\text{CDK1}$, $\text{Plk1}$, $\text{APK}$) ([Ferrell *et al.*, 2011][ferrell-2011]) (see `CellTypes` → `CellType` → `System`). This ODE model is coupled to 2D shaped CPM cells that perform divide based on the concentration of these components (see `CellTypes` → `CellType` → `Proliferation` → ```Condition```). As in the early *Xenopus* cell cleavage, this leads to exponential growth of the number of cells, without increase of total cell volume.

### Time scales

Time scales are defined in the following fashion:

- The so-called global time scheme is defined in `Time` and here runs from $0$ to $1$ arbitrary time units. All models and plugins specify their updating scheme in terms this global time scheme (e.g. `Analysis` → `Gnuplotter` → `Interval`).
- The CPM time scale for cell motility and behaviours is defined in `CPM` → `MCSDuration`. This specifies the time that a single Monte Carlo step in the CPM lasts, in terms of the 'global time'. Here, the `MCSDuration` is $1.0\cdot10^{-4}$ which means the CPM is executed 10.000 times during this simulation.
- For setting time of ODEs, one has to distinguish the (1) how often the ODEs are evaluated from (2) controlling the time scale of the ODE dynamics:
    1. The time scale of the ODE dynamics can be changed using `System` → `time-scaling`. When larger or smaller than `1.0`, this speeds up or slows down the dynamics without influencing the accuracy of the approximation.
    1. The accuracy of the numerical approximation (and is equal to the $\Delta t$ of the numerical solver) is controlled using `System` → `time-step` (and is automatically rescaled according to the time scale).

## Things to Try

- Change the CPM time scale, relative to the ODE dynamics: Change `CPM` → `MCSDuration` to $1.0\cdot10^{-3}$ or decrease to $1.0\cdot10^{-5}$. This makes cells to have less resp. more motility/relaxation in between cell divisions.
- Change the time scale of the ODE dynamics relative to the CPM by altering `System` → `time-scaling`.

## Reference

>J. E. Ferrell Jr., T. Y. Tsai, Q. Yang: [Modeling the Cell Cycle: Why Do Certain Circuits Oscillate?][ferrell-2011]. *Cell* **144** (6): 874-885, 2011.

[ferrell-2011]: http://dx.doi.org/10.1016/j.cell.2011.03.006