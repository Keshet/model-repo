---
MorpheusModelID: M0043

title: "Minimal Model"
date: "2019-11-10T09:49:00+01:00"
lastmod: "2020-10-30T13:50:00+01:00"

aliases: [/examples/minimal-model/]

menu:
  Built-in Examples:
    parent: Miscellaneous Models
    weight: 40
weight: 540
---
## Introduction

This example does nothing -- expect being the minimal valid Morpheus model. Such a model is generated when choosing ```File``` → ```New```.

{{< figure library="true" src="examples/miscellaneous/minimal_model.png" lightbox="true" title="Minimal valid XML model." >}}

## Description

The basic model only includes the required nodes ```MorpheusModel```, ```Description```, ```Space``` and ```Time```. Their required nodes and attributes are added recursively, such as ```Lattice``` class and ```StopTime``` value.

## Things to try

- Invalidate this minimal model by editing it (but keeping it well-formed). When opening this model in Morpheus GUI, it triggers a warning saying what went wrong and how it was solved. Check the ```Fixboard``` to see the changes that were made to the model.

## More information

- FAQ: [Your First Model](/faq/modeling/first-model/)